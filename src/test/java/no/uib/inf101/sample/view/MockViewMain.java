package no.uib.inf101.sample.view;

import javax.swing.JFrame;

import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.model.Person;
import no.uib.inf101.sample.model.PersonList;

public class MockViewMain {
    
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    PersonList model = new PersonList();
    for (int i = 0; i < 10; i++) {
      model.addPerson(new Person("Adam", i));
      model.addPerson(new Person("Eva", i));
    }
    
    ViewMain view = new ViewMain(model, eventBus);
    
    JFrame frame = new JFrame("MOCK");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
}
